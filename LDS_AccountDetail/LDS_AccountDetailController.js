({
    // Loading the account on click of account name
	accountOpenEt : function(component, event, helper) {
		var recordId = event.getParam("recordId");
        var tpyeAction = event.getParam("tpyeAction");
        component.set("v.recordId",recordId);
        component.find("recordLoader").reloadRecord();
	},
    editAccountWindow : function (component, event, helper) {
        component.set("v.displayMode",'EDIT');
    },
    saveButton : function(component,event,helper){
        component.find("recordLoader").saveRecord($A.getCallback(function(saveResult) {
            // NOTE: If you want a specific behavior(an action or UI behavior) when this action is successful
            // then handle that in a callback (generic logic when record is changed should be handled in recordUpdated event handler)
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                // handle component related logic in event handler
                component.set("v.displayMode",'VIEW');
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        }));
    },
    cancelButton : function(component,event,helper){
        component.set("v.displayMode",'VIEW');
    },

    // This method will fire everytime account details are changed
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "CHANGED") {
            // get the fields that changed for this record
        } else if(eventParams.changeType === "LOADED") {
            // record is loaded in the cache
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted and removed from the cache
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving or deleting the record
        }
    },

    newAccountForm : function (component,event,helper) {
        component.find("recordLoader").getNewRecord(
            "Account", // sObject type (objectApiName)
            null,      // recordTypeId
            false,     // skip cache?
            $A.getCallback(function() {
                var rec = component.get("v.simpleRecord");
                var error = component.get("v.recordError");
                if(error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                    return;
                }
                component.set("v.displayMode",'EDIT');
                console.log("Record template initialized: " + rec.sobjectType);
            })
        );
    }
})