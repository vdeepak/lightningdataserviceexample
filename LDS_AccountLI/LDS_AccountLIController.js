({
	openAccountDetail : function(component, event, helper) {
		var myEvent = $A.get("e.c:LDS_AccountOpen");
        var acc = component.get("v.account");
        myEvent.setParams({ "recordId": acc.Id , "tpyeAction" : "recordDetail"});
        myEvent.fire();
	}
})