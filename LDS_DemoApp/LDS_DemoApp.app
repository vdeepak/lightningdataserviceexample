<aura:application access="GLOBAL" extends="force:slds" implements="force:appHostable,flexipage:availableForAllPageTypes"
                  controller="AccountListCntrl">
    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>
    <aura:handler event="c:LDS_AccountOpen" action="{!c.showAccount}"/>
    <aura:attribute name="accounts" type="Account[]"/>
    <aura:attribute name="truthy" type="String" default="none"/>
    <aura:registerEvent name="newAccountFrom" type="c:LDS_NewAccount"  />
    
    <div class="slds">
        <div class="slds-grid slds-wrap slds-grid_pull-padded">
            <div class="slds-p-horizontal_small slds-size_1-of-2 slds-medium-size_1-of-6 slds-large-size_4-of-12">
                <table class="slds-table slds-table_bordered slds-table_cell-buffer slds-border_right">
                    <thead>
                    <tr>
                        <th scope="col">Account Name
                            <lightning:buttonIcon class="buttonDir" iconName="utility:add" variant="border" onclick="{!c.addAccountWindow}" alternativeText="Close window"/>
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    <aura:iteration items="{!v.accounts}" var="acc">
                        <tr class="slds-hint-parent">
                            <c:LDS_AccountLI account="{!acc}"/>
                        </tr>
                    </aura:iteration>
                    </tbody>
                </table>
            </div>
            <div class="slds-p-horizontal_small slds-size_1-of-2 slds-medium-size_5-of-6 slds-large-size_8-of-12">
                <aura:if isTrue="{!v.truthy == 'none'}">
                    <div class="slds-notify slds-notify_alert slds-theme_alert-texture slds-theme_offline" role="alert">
                        <span class="slds-assistive-text">offline</span>
                        <h2>Please choose some action</h2>
                    </div>
                </aura:if>
                <aura:if isTrue="{!v.truthy == 'recordDetail'}">
                    <c:LDS_AccountDetail />
                </aura:if>
            </div>
        </div>
    </div>
</aura:application>