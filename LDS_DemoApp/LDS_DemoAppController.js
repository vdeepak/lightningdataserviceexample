({
	doInit : function(component, event, helper) {
		helper.getAllAccounts(component);			
	},
    showAccount : function(component, event, helper) {
        var recordId = event.getParam("recordId");
        var tpyeAction = event.getParam("tpyeAction");
        component.set("v.truthy",tpyeAction);
        
    },
    addAccountWindow : function(component, event, helper){
        component.set("v.truthy",'recordDetail');
	    var myEvent = $A.get("e.c:LDS_NewAccount");
        myEvent.fire();
    }
})