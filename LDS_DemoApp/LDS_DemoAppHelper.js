({
	getAllAccounts : function(component) {
		var action = component.get("c.getListOfAccount");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.accounts", response.getReturnValue());                
            }
        });
        $A.enqueueAction(action);
	}
})